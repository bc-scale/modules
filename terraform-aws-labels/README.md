## Terraform Labels
This terraform module is designed to generate consistent label names and tags for resources. You can use terraform-labels to implement a strict naming convention.


## Examples

### Simple Example
Here is an example of how you can use this module in your inventory structure:
```hcl
    module "label" {
          source      = "git::https://github.com/clouddrove/terraform-labels.git?ref=tags/0.15.0"
          name        = "labels"
          environment = "prod"
          managedby   = "hello@clouddrove.com"
          repository  = "https://github.com/clouddrove/terraform-labels"
          label_order = ["name","attributes","environment"]
          delimiter   = "-"
      tags = {
          "Terraform Version"   = "1.0.1"
          "created_date"        = "4-Apr-21"
      }
   }
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| attributes | Additional attributes (e.g. `1`). | `list(string)` | `[]` | no |
| delimiter | Delimiter to be used between `organization`, `name`, `environment` and `attributes`. | `string` | `"-"` | no |
| enabled | Set to false to prevent the module from creating any resources. | `bool` | `true` | no |
| environment | Environment (e.g. `prod`, `dev`, `staging`). | `string` | `""` | no |
| extra\_tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`). | `map(string)` | `{}` | no |
| label\_order | Label order, e.g. sequence of application name and environment `name`,`environment`,'attribute' [`webserver`,`qa`,`devops`,`public`,] . | `list(any)` | `[]` | no |
| managedby | ManagedBy, eg 'CloudDrove'. | `string` | `"DenAV"` | no |
| name | Name  (e.g. `app` or `cluster`). | `string` | `""` | no |
| repository | Terraform current module repo | `string` | `"https://github.com/..."` | no |

## Outputs

| Name | Description |
|------|-------------|
| attributes | Normalized attributes. |
| environment | Normalized environment |
| id | Disambiguated ID. |
| label\_order | Normalized Tag map. |
| name | Normalized name. |
| repository | Terraform current module repo |
| tags | Normalized Tag map. |

### Resource
[clouddrove](https://github.com/clouddrove/terraform-aws-labels)