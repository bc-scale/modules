# -------------------------------------------------------------------------------
# Provides a Hetzner DNS Recrods resource to create, update and delete DNS Records.

terraform {
  required_providers {
    hetznerdns = {
      source = "timohirt/hetznerdns"
      version = "2.1.0"
    }
  }
  required_version = ">= 1.0"
}


data "hetznerdns_zone" "dns_zone" {
    name = var.dns_zone
}

 
resource "hetznerdns_record" "web" {
    zone_id = data.hetznerdns_zone.dns_zone.id
    name    = var.dns_name
    value   = var.dns_value
    type    = var.dns_type
    ttl     = var.dns_ttl
}
