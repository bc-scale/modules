# ------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ------------------------------------------------------------------------------

variable "dns_zone" {
  description = "(Required, string) Name of the DNS zone to create the record in."
  type        = string
}

variable "dns_name" {
  description = "(Required, string) Name of the DNS record to create."
  type        = string
}

variable "dns_value" {
  description = "(Required, string) The value of the record."
  type        = string
}

variable "dns_type" {
  description = "(Required, string) The type of the record."
  type        = string
}

variable "dns_ttl" {
  description = "(Optional, int) Time to live of this record."
  type        = number
}
