locals {
  default_launch_configuration_id   = "${var.launch_configuration == "" && var.create_lc ? element(aws_launch_configuration.default.*.id, 0) : var.launch_configuration}"
  default_launch_configuration_name = "${var.launch_configuration == "" && var.create_lc ? element(aws_launch_configuration.default.*.name, 0) : ""}"

  default_autoscaling_group_id                        = element(coalescelist(aws_autoscaling_group.default.*.id), 0)
  default_autoscaling_group_name                      = element(coalescelist(aws_autoscaling_group.default.*.name), 0)
  default_autoscaling_group_arn                       = element(coalescelist(aws_autoscaling_group.default.*.arn), 0)
  default_autoscaling_group_min_size                  = element(coalescelist(aws_autoscaling_group.default.*.min_size), 0)
  default_autoscaling_group_max_size                  = element(coalescelist(aws_autoscaling_group.default.*.max_size), 0)
  default_autoscaling_group_desired_capacity          = element(coalescelist(aws_autoscaling_group.default.*.desired_capacity), 0)
  default_autoscaling_group_default_cooldown          = element(coalescelist(aws_autoscaling_group.default.*.default_cooldown), 0)
  default_autoscaling_group_health_check_grace_period = element(coalescelist(aws_autoscaling_group.default.*.health_check_grace_period), 0)
  default_autoscaling_group_health_check_type         = element(coalescelist(aws_autoscaling_group.default.*.health_check_type), 0)
}

output "default_launch_configuration_id" {
  description = "The ID of the launch configuration"
  value       = local.default_launch_configuration_id
}

output "default_launch_configuration_name" {
  description = "The name of the launch configuration"
  value       = local.default_launch_configuration_name
}

output "default_autoscaling_group_id" {
  description = "The autoscaling group id"
  value       = local.default_autoscaling_group_id
}

output "default_autoscaling_group_name" {
  description = "The autoscaling group name"
  value       = local.default_autoscaling_group_name
}

output "default_autoscaling_group_arn" {
  description = "The ARN for this AutoScaling Group"
  value       = local.default_autoscaling_group_arn
}

output "default_autoscaling_group_min_size" {
  description = "The minimum size of the autoscale group"
  value       = local.default_autoscaling_group_min_size
}

output "default_autoscaling_group_max_size" {
  description = "The maximum size of the autoscale group"
  value       = local.default_autoscaling_group_max_size
}

output "default_autoscaling_group_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"
  value       = local.default_autoscaling_group_desired_capacity
}

output "default_autoscaling_group_default_cooldown" {
  description = "Time between a scaling activity and the succeeding scaling activity"
  value       = local.default_autoscaling_group_default_cooldown
}

output "default_autoscaling_group_health_check_grace_period" {
  description = "Time after instance comes into service before checking health"
  value       = local.default_autoscaling_group_health_check_grace_period
}

output "default_autoscaling_group_health_check_type" {
  description = "EC2 or ELB. Controls how health checking is done"
  value       = local.default_autoscaling_group_health_check_type
}

output "tags" {
  value       = module.labels.tags
  description = "The instance ID."
}

//output "default_autoscaling_group_vpc_zone_identifier" {
//  description = "The VPC zone identifier"
//  value       = element(aws_autoscaling_group.default.vpc_zone_identifier, 0)
//}
//
//output "default_autoscaling_group_load_balancers" {
//  description = "The load balancer names associated with the autoscaling group"
//  value       = aws_autoscaling_group.default.load_balancers
//}
//
//output "default_autoscaling_group_target_group_arns" {
//  description = "List of Target Group ARNs that apply to this AutoScaling Group"
//  value       = aws_autoscaling_group.default.target_group_arns
//}
